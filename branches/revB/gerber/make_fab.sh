#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-FT-revB"
mv feedthrough-B_Cu.pho ${base}-layer_8.pho
mv feedthrough-B_Mask.pho ${base}-B_Mask.pho
mv feedthrough-B_Paste.pho ${base}-B_Paste.pho
mv feedthrough-B_SilkS.pho ${base}-B_SilkS.pho
mv feedthrough-Cmts_User.pho ${base}-Fab_Dwg.pho
mv feedthrough.drl ${base}.drl
mv feedthrough-drl_map.pho ${base}-drl_map.pho
mv feedthrough-F_Cu.pho ${base}-layer_1.pho
mv feedthrough-F_Mask.pho ${base}-F_Mask.pho
mv feedthrough-F_Paste.pho ${base}-F_Paste.pho
mv feedthrough-F_SilkS.pho ${base}-F_SilkS.pho
mv feedthrough-Inner1_Cu.pho ${base}-layer_3.pho
mv feedthrough-Inner2_Cu.pho ${base}-layer_4.pho
mv feedthrough-Inner3_Cu.pho ${base}-layer_5.pho
mv feedthrough-Inner4_Cu.pho ${base}-layer_6.pho
mv feedthrough-Inner5_Cu.pho ${base}-layer_2.pho
mv feedthrough-Inner6_Cu.pho ${base}-layer_7.pho
mv feedthrough-NPTH.drl ${base}-NPTH.drl
mv feedthrough-NPTH-drl_map.pho ${base}-NPTH-drl_map.pho

