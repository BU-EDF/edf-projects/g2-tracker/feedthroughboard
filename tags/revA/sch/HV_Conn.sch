EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:erf8-050-05
LIBS:feedthrough-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title ""
Date "12 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5150 2800 6550 2800
Wire Wire Line
	5150 5200 6550 5200
Text Label 5700 2800 0    60   ~ 0
HV_0
Text Label 5700 3200 0    60   ~ 0
GND
Text Label 5700 3800 0    60   ~ 0
HV_1
Text Label 5700 3400 0    60   ~ 0
GND
Text Label 5700 4200 0    60   ~ 0
HV_2
Text Label 5700 4600 0    60   ~ 0
GND
Text Label 5700 5200 0    60   ~ 0
HV_3
Text Label 5700 4800 0    60   ~ 0
GND
Wire Wire Line
	5150 3200 6550 3200
Wire Wire Line
	5150 3800 6550 3800
Wire Wire Line
	5150 4200 6550 4200
Wire Wire Line
	5150 4600 6550 4600
Wire Wire Line
	5150 4800 6550 4800
$Comp
L BOARD_CONNECT J9
U 1 1 54651E90
P 4850 2950
F 0 "J9" H 5250 2750 60  0001 C CNN
F 1 "BOARD_CONNECT" H 4850 3200 60  0001 C CNN
F 2 "~" H 4850 2950 60  0000 C CNN
F 3 "~" H 4850 2950 60  0000 C CNN
F 4 "S1033E-25-ND" H 4850 2950 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 4850 2950 60  0001 C CNN "Field2"
	1    4850 2950
	1    0    0    -1  
$EndComp
$Comp
L BOARD_CONNECT J10
U 1 1 54651EA9
P 6850 5050
F 0 "J10" H 7250 4850 60  0001 C CNN
F 1 "BOARD_CONNECT" H 6850 5300 60  0001 C CNN
F 2 "~" H 6850 5050 60  0000 C CNN
F 3 "~" H 6850 5050 60  0000 C CNN
F 4 "S1033E-25-ND" H 6850 5050 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 6850 5050 60  0001 C CNN "Field2"
	1    6850 5050
	-1   0    0    1   
$EndComp
NoConn ~ 6550 4900
NoConn ~ 6550 4400
NoConn ~ 6550 4300
NoConn ~ 5150 4900
NoConn ~ 5150 4400
NoConn ~ 5150 4300
NoConn ~ 5150 4000
NoConn ~ 5150 3900
NoConn ~ 6550 4000
NoConn ~ 6550 3900
NoConn ~ 6550 3600
NoConn ~ 6550 3500
NoConn ~ 5150 3600
NoConn ~ 5150 3500
NoConn ~ 5150 3100
NoConn ~ 5150 3000
NoConn ~ 5150 2900
NoConn ~ 6550 3100
NoConn ~ 6550 3000
NoConn ~ 6550 2900
Wire Wire Line
	6550 3400 5150 3400
NoConn ~ 5150 3300
NoConn ~ 6550 3300
NoConn ~ 5150 5100
NoConn ~ 6550 5100
NoConn ~ 5150 5000
NoConn ~ 6550 5000
NoConn ~ 5150 4500
NoConn ~ 6550 4500
NoConn ~ 5150 4700
NoConn ~ 6550 4700
NoConn ~ 5150 4100
NoConn ~ 6550 4100
NoConn ~ 5150 3700
NoConn ~ 6550 3700
$EndSCHEMATC
