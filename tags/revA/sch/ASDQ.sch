EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:erf8-050-05
LIBS:feedthrough-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date "12 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5200 6400 0    60   Output ~ 0
+1.4VRet
Text HLabel 5200 6500 0    60   Input ~ 0
+1.4V
Text HLabel 5200 7100 0    60   Output ~ 0
-3VRet
Text HLabel 5200 7200 0    60   Input ~ 0
-3V
Text HLabel 5200 6700 0    60   Output ~ 0
+3VRet
Text HLabel 5200 6800 0    60   Input ~ 0
+3V
Text HLabel 4600 900  0    60   Input ~ 0
VRef[3..0]
Text HLabel 4600 1800 0    60   Input ~ 0
TP2_[1..0]
Text HLabel 4600 1900 0    60   Input ~ 0
TP1_[1..0]
Text HLabel 4600 1400 0    60   BiDi ~ 0
TEMP
Text HLabel 3200 4500 0    60   Output ~ 0
AP[7..0]
Text HLabel 3200 4400 0    60   Output ~ 0
AN[7..0]
Text HLabel 3200 4300 0    60   Output ~ 0
ShieldA
Text HLabel 3200 2100 0    60   Output ~ 0
ShieldB
Text HLabel 3200 2300 0    60   Output ~ 0
BP[7..0]
Text HLabel 3200 2200 0    60   Output ~ 0
BN[7..0]
$Comp
L FH40-64S-0.5SV J4
U 1 1 544A851D
P 6450 4150
AR Path="/542B5663/544A851D" Ref="J4"  Part="1" 
AR Path="/542B5662/544A851D" Ref="J3"  Part="1" 
AR Path="/542B52D3/544A851D" Ref="J2"  Part="1" 
AR Path="/542B50F4/544A851D" Ref="J6"  Part="1" 
F 0 "J2" H 6400 850 60  0000 C CNN
F 1 "FH40-64S-0.5SV" V 6450 4150 60  0000 C CNN
F 2 "~" H 6700 4150 60  0000 C CNN
F 3 "~" H 6700 4150 60  0000 C CNN
F 4 "HFW64CT-ND" H 6450 4150 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 6450 4150 60  0001 C CNN "Field2"
	1    6450 4150
	1    0    0    -1  
$EndComp
Text Label 3200 6200 2    60   ~ 0
ShieldA
Text Label 5300 2300 0    60   ~ 0
BN7
Entry Wire Line
	5200 2200 5300 2300
Entry Wire Line
	5000 2200 5100 2300
Entry Wire Line
	4800 2200 4900 2300
Entry Wire Line
	4600 2200 4700 2300
Entry Wire Line
	4400 2200 4500 2300
Entry Wire Line
	4200 2200 4300 2300
Entry Wire Line
	4000 2200 4100 2300
Entry Wire Line
	3800 2200 3900 2300
Text Label 5100 2500 0    60   ~ 0
BN6
Text Label 4900 2700 0    60   ~ 0
BN5
Text Label 4700 2900 0    60   ~ 0
BN4
Text Label 4500 3100 0    60   ~ 0
BN3
Text Label 4300 3300 0    60   ~ 0
BN2
Text Label 4100 3500 0    60   ~ 0
BN1
Text Label 3900 3700 0    60   ~ 0
BN0
Entry Wire Line
	3400 2300 3500 2400
Entry Wire Line
	3400 2500 3500 2600
Entry Wire Line
	3400 2700 3500 2800
Entry Wire Line
	3400 2900 3500 3000
Entry Wire Line
	3400 3100 3500 3200
Entry Wire Line
	3400 3300 3500 3400
Entry Wire Line
	3400 3500 3500 3600
Entry Wire Line
	3400 3700 3500 3800
Text Label 3500 2400 0    60   ~ 0
BP7
Text Label 3500 2600 0    60   ~ 0
BP6
Text Label 3500 2800 0    60   ~ 0
BP5
Text Label 3500 3000 0    60   ~ 0
BP4
Text Label 3500 3200 0    60   ~ 0
BP3
Text Label 3500 3400 0    60   ~ 0
BP2
Text Label 3500 3600 0    60   ~ 0
BP1
Text Label 3500 3800 0    60   ~ 0
BP0
Text Label 3200 2000 2    60   ~ 0
ShieldB
Text Label 3200 4000 2    60   ~ 0
ShieldB
Text Label 3200 4200 2    60   ~ 0
ShieldA
Entry Wire Line
	5200 4400 5300 4500
Entry Wire Line
	5000 4400 5100 4500
Entry Wire Line
	4800 4400 4900 4500
Entry Wire Line
	4600 4400 4700 4500
Entry Wire Line
	4400 4400 4500 4500
Entry Wire Line
	4200 4400 4300 4500
Entry Wire Line
	4000 4400 4100 4500
Entry Wire Line
	3800 4400 3900 4500
Wire Wire Line
	5200 7200 5900 7200
Wire Wire Line
	5200 7100 5900 7100
Wire Wire Line
	5200 6800 5900 6800
Wire Wire Line
	5900 6900 5700 6900
Wire Wire Line
	5700 6900 5700 6800
Connection ~ 5700 6800
Wire Wire Line
	5200 6700 5900 6700
Wire Wire Line
	5900 6600 5700 6600
Wire Wire Line
	5700 6600 5700 6700
Connection ~ 5700 6700
Wire Wire Line
	5900 7300 5700 7300
Wire Wire Line
	5700 7300 5700 7200
Connection ~ 5700 7200
Wire Wire Line
	5900 7000 5700 7000
Wire Wire Line
	5700 7000 5700 7100
Connection ~ 5700 7100
Wire Wire Line
	5200 6500 5900 6500
Wire Wire Line
	5200 6400 5900 6400
Wire Wire Line
	3200 6200 5900 6200
Wire Wire Line
	5900 6300 5700 6300
Wire Wire Line
	5700 6300 5700 6200
Connection ~ 5700 6200
Wire Wire Line
	5900 2300 5300 2300
Wire Bus Line
	3200 2200 5200 2200
Wire Wire Line
	5100 2300 5100 2500
Wire Wire Line
	5100 2500 5900 2500
Wire Wire Line
	4900 2300 4900 2700
Wire Wire Line
	4900 2700 5900 2700
Wire Wire Line
	4700 2300 4700 2900
Wire Wire Line
	4700 2900 5900 2900
Wire Wire Line
	4500 2300 4500 3100
Wire Wire Line
	4500 3100 5900 3100
Wire Wire Line
	4300 2300 4300 3300
Wire Wire Line
	4300 3300 5900 3300
Wire Wire Line
	4100 2300 4100 3500
Wire Wire Line
	4100 3500 5900 3500
Wire Wire Line
	3900 2300 3900 3700
Wire Wire Line
	3900 3700 5900 3700
Wire Bus Line
	3200 2300 3400 2300
Wire Bus Line
	3400 2300 3400 3700
Wire Wire Line
	3500 2400 5900 2400
Wire Wire Line
	3500 2600 5900 2600
Wire Wire Line
	3500 2800 5900 2800
Wire Wire Line
	3500 3000 5900 3000
Wire Wire Line
	3500 3200 5900 3200
Wire Wire Line
	3500 3400 5900 3400
Wire Wire Line
	3500 3600 5900 3600
Wire Wire Line
	3500 3800 5900 3800
Wire Wire Line
	3200 2100 5900 2100
Wire Wire Line
	5900 2200 5700 2200
Wire Wire Line
	5700 2200 5700 2100
Connection ~ 5700 2100
Wire Wire Line
	3200 2000 3400 2000
Wire Wire Line
	3400 2000 3400 2100
Connection ~ 3400 2100
Wire Wire Line
	3200 4000 5900 4000
Wire Wire Line
	5900 4100 5700 4100
Wire Wire Line
	5700 4100 5700 4000
Connection ~ 5700 4000
Wire Wire Line
	3200 4300 5900 4300
Wire Wire Line
	5900 4400 5700 4400
Wire Wire Line
	5700 4400 5700 4300
Connection ~ 5700 4300
Wire Wire Line
	3200 4200 3400 4200
Wire Wire Line
	3400 4200 3400 4300
Connection ~ 3400 4300
Wire Bus Line
	3200 4400 5200 4400
Wire Wire Line
	5300 4500 5900 4500
Wire Wire Line
	5100 4500 5100 4700
Wire Wire Line
	5100 4700 5900 4700
Wire Wire Line
	4900 4500 4900 4900
Wire Wire Line
	4900 4900 5900 4900
Wire Wire Line
	4700 4500 4700 5100
Wire Wire Line
	4700 5100 5900 5100
Wire Wire Line
	4500 4500 4500 5300
Wire Wire Line
	4500 5300 5900 5300
Wire Wire Line
	4300 4500 4300 5500
Wire Wire Line
	4300 5500 5900 5500
Wire Wire Line
	4100 4500 4100 5700
Wire Wire Line
	4100 5700 5900 5700
Wire Wire Line
	3900 4500 3900 5900
Wire Wire Line
	3900 5900 5900 5900
Wire Wire Line
	5900 6100 5800 6100
Wire Wire Line
	5800 6100 5800 6200
Connection ~ 5800 6200
Wire Wire Line
	5800 4300 5800 4200
Wire Wire Line
	5800 4200 5900 4200
Connection ~ 5800 4300
Wire Wire Line
	5800 4000 5800 3900
Wire Wire Line
	5800 3900 5900 3900
Connection ~ 5800 4000
Wire Bus Line
	3200 4500 3450 4500
Wire Bus Line
	3450 4500 3450 5900
Entry Wire Line
	3450 4500 3550 4600
Entry Wire Line
	3450 4700 3550 4800
Entry Wire Line
	3450 4900 3550 5000
Entry Wire Line
	3450 5100 3550 5200
Entry Wire Line
	3450 5300 3550 5400
Entry Wire Line
	3450 5500 3550 5600
Entry Wire Line
	3450 5700 3550 5800
Entry Wire Line
	3450 5900 3550 6000
Wire Wire Line
	3550 4600 5900 4600
Wire Wire Line
	3550 4800 5900 4800
Wire Wire Line
	3550 5000 5900 5000
Wire Wire Line
	3550 5200 5900 5200
Wire Wire Line
	3550 5400 5900 5400
Wire Wire Line
	3550 5600 5900 5600
Wire Wire Line
	3550 5800 5900 5800
Wire Wire Line
	3550 6000 5900 6000
Text Label 5300 4500 0    60   ~ 0
AN7
Text Label 5100 4700 0    60   ~ 0
AN6
Text Label 4900 4900 0    60   ~ 0
AN5
Text Label 4700 5100 0    60   ~ 0
AN4
Text Label 4500 5300 0    60   ~ 0
AN3
Text Label 4300 5500 0    60   ~ 0
AN2
Text Label 4100 5700 0    60   ~ 0
AN1
Text Label 3900 5900 0    60   ~ 0
AN0
Text Label 3550 4600 0    60   ~ 0
AP7
Text Label 3550 4800 0    60   ~ 0
AP6
Text Label 3550 5000 0    60   ~ 0
AP5
Text Label 3550 5200 0    60   ~ 0
AP4
Text Label 3550 5400 0    60   ~ 0
AP3
Text Label 3550 5600 0    60   ~ 0
AP2
Text Label 3550 5800 0    60   ~ 0
AP1
Text Label 3550 6000 0    60   ~ 0
AP0
Text Notes 3800 1800 0    60   ~ 0
0=N\n1=P
Entry Wire Line
	5200 1900 5300 1800
Text Label 5500 1900 0    60   ~ 0
TP1_1
Text Label 5300 1800 0    60   ~ 0
TP1_0
Entry Wire Line
	5000 1800 5100 1700
Entry Wire Line
	4800 1800 4900 1700
Text Label 5100 1700 0    60   ~ 0
TP2_1
Text Label 4900 1600 0    60   ~ 0
TP2_0
Wire Wire Line
	4900 1700 4900 1600
Wire Wire Line
	3400 1500 5900 1500
Wire Wire Line
	4600 1400 5900 1400
Wire Bus Line
	4600 900  5400 900 
Entry Wire Line
	5400 900  5500 1000
Entry Wire Line
	5200 900  5300 1000
Entry Wire Line
	5000 900  5100 1000
Entry Wire Line
	4800 900  4900 1000
Wire Wire Line
	5500 1000 5900 1000
Wire Wire Line
	5300 1000 5300 1100
Wire Wire Line
	5300 1100 5900 1100
Wire Wire Line
	5100 1000 5100 1200
Wire Wire Line
	5100 1200 5900 1200
Wire Wire Line
	4900 1000 4900 1300
Wire Wire Line
	4900 1300 5900 1300
Text Label 5500 1000 0    60   ~ 0
VRef0
Text Label 5300 1100 0    60   ~ 0
VRef1
Text Label 5100 1200 0    60   ~ 0
VRef2
Text Label 4900 1300 0    60   ~ 0
VRef3
Text Notes 4400 1050 0    60   ~ 0
IBLR
Text Notes 4400 1150 0    60   ~ 0
DTHR
Text Notes 4400 1250 0    60   ~ 0
TREFE_IN
Text Notes 4400 1350 0    60   ~ 0
TREF0_IN
Wire Bus Line
	4600 1900 5500 1900
Wire Bus Line
	4600 1800 5000 1800
Wire Wire Line
	4900 1600 5900 1600
Wire Wire Line
	5900 1700 5100 1700
Wire Wire Line
	5300 1800 5900 1800
Wire Wire Line
	5900 1900 5500 1900
Wire Wire Line
	5900 2000 3650 2000
Wire Wire Line
	3650 2000 3650 1500
Connection ~ 3650 1500
Text HLabel 3400 1500 0    60   UnSpc ~ 0
Vref_gnd
$EndSCHEMATC
