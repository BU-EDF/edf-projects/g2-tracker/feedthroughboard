#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-FT"
cp feedthrough-B_Cu.pho ${base}-layer_8.pho
cp feedthrough-B_Mask.pho ${base}-B_Mask.pho
cp feedthrough-B_Paste.pho ${base}-B_Paste.pho
cp feedthrough-B_SilkS.pho ${base}-B_SilkS.pho
cp feedthrough-Cmts_User.pho ${base}-Fab_Dwg.pho
cp feedthrough.drl ${base}.drl
cp feedthrough-drl_map.pho ${base}-drl_map.pho
cp feedthrough-F_Cu.pho ${base}-layer_1.pho
cp feedthrough-F_Mask.pho ${base}-F_Mask.pho
cp feedthrough-F_Paste.pho ${base}-F_Paste.pho
cp feedthrough-F_SilkS.pho ${base}-F_SilkS.pho
cp feedthrough-Inner1_Cu.pho ${base}-layer_3.pho
cp feedthrough-Inner2_Cu.pho ${base}-layer_4.pho
cp feedthrough-Inner3_Cu.pho ${base}-layer_5.pho
cp feedthrough-Inner4_Cu.pho ${base}-layer_6.pho
cp feedthrough-Inner5_Cu.pho ${base}-layer_2.pho
cp feedthrough-Inner6_Cu.pho ${base}-layer_7.pho
cp feedthrough-NPTH.drl ${base}-NPTH.drl
cp feedthrough-NPTH-drl_map.pho ${base}-NPTH-drl_map.pho

