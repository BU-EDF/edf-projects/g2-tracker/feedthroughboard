Boston University P/N G2-FT Rev A  2015-01-14

8 Layers
Final thickness 0.092 in
PLUG VIAS WITH EPOXY FILL
All copper 1/2 oz initial
Surface finish ENIG


G2-FT-B_Mask.pho		Bottom solder mask
G2-FT-B_Paste.pho		Bottom solder paste
G2-FT-B_SilkS.pho		Bottom silkscreen legent
G2-FT.drl			NC drill
G2-FT-drl_map.pho		NC drill map
G2-FT-Fab_Dwg.pho		Fabrication drawing
G2-FT-F_Mask.pho		Top solder mask
G2-FT-F_Paste.pho		Top solder paste
G2-FT-F_SilkS.pho		Top silkscreen
G2-FT-layer_1.pho		Layer 1 (signal) - TOP
G2-FT-layer_2.pho		Layer 2 (power, positive)
G2-FT-layer_3.pho		Layer 3 (signal)
G2-FT-layer_4.pho		Layer 4 (signal)
G2-FT-layer_5.pho		Layer 5 (signal)
G2-FT-layer_6.pho		Layer 6 (signal)
G2-FT-layer_7.pho		Layer 7 (power, positive)
G2-FT-layer_8.pho		Layer 8 (signal) - BOTTOM
G2-FT-NPTH.drl			NC drill non-plated holes
G2-FT-NPTH-drl_map.pho		NC drill map non-plated holes

Technical contact:  Dan Gastler <dgastler@bu.edu>  218-461-9178
Administrative contact:  Chris Lawlor <cjlawlor@bu.edu>  617-353-4117

