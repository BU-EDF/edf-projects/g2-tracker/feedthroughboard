EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:erf8-050-05
LIBS:feedthrough-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title ""
Date "12 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8050 3550 2    60   Output ~ 0
JTAG1_TCK
Text HLabel 8150 4750 2    60   Output ~ 0
+3V[4..1]
Text HLabel 3750 4750 0    60   Input ~ 0
+3VRet[4..1]
Text HLabel 8150 4350 2    60   Output ~ 0
-3V[4..1]
Text HLabel 3750 4350 0    60   Input ~ 0
-3VRet[4..1]
Text HLabel 6900 6550 2    60   Output ~ 0
+4V
Text HLabel 4900 6550 0    60   Input ~ 0
+4VRet
Text HLabel 8050 1950 2    60   Output ~ 0
VRef_ASDQ1_[3..0]
Text HLabel 8050 2350 2    60   Output ~ 0
VRef_ASDQ2_[3..0]
Text HLabel 3850 1950 0    60   Output ~ 0
VRef_ASDQ3_[3..0]
Text HLabel 3850 2350 0    60   Output ~ 0
VRef_ASDQ4_[3..0]
Text HLabel 3850 3550 0    60   Output ~ 0
JTAG2_TCK
Text HLabel 3850 3750 0    60   Input ~ 0
JTAG2_TDO
Text HLabel 3850 3850 0    60   Output ~ 0
JTAG2_TMS
Text HLabel 3850 3650 0    60   Output ~ 0
JTAG2_TDI
Text HLabel 8050 3750 2    60   Input ~ 0
JTAG1_TDO
Text HLabel 8050 3850 2    60   Output ~ 0
JTAG1_TMS
Text HLabel 8050 3650 2    60   Output ~ 0
JTAG1_TDI
Text HLabel 8050 1550 2    60   Output ~ 0
TDC1_C5_[1..0]
Text HLabel 8050 1750 2    60   Output ~ 0
TDC2_C5_[1..0]
Text HLabel 3850 1550 0    60   Output ~ 0
TDC3_C5_[1..0]
Text HLabel 3850 1750 0    60   Output ~ 0
TDC4_C5_[1..0]
Text HLabel 8050 3050 2    60   Input ~ 0
TDC1_8B10B_[1..0]
Text HLabel 8050 3250 2    60   Input ~ 0
TDC2_8B10B_[1..0]
Text HLabel 3850 3050 0    60   Input ~ 0
TDC3_8B10B_[1..0]
Text HLabel 3850 3250 0    60   Input ~ 0
TDC4_8B10B_[1..0]
Text HLabel 5100 2950 0    60   BiDi ~ 0
TEMP_0
Text HLabel 10050 2900 2    60   UnSpc ~ 0
Vref_gnd
Text HLabel 8150 5450 2    60   Output ~ 0
ASDQ1_TP1_[1..0]
Text HLabel 8150 5650 2    60   Output ~ 0
ASDQ1_TP2_[1..0]
Text HLabel 8150 5050 2    60   Output ~ 0
ASDQ2_TP1_[1..0]
Text HLabel 8150 5250 2    60   Output ~ 0
ASDQ2_TP2_[1..0]
Text HLabel 3750 5450 0    60   Output ~ 0
ASDQ3_TP1_[1..0]
Text HLabel 3750 5650 0    60   Output ~ 0
ASDQ3_TP2_[1..0]
Text HLabel 3750 5050 0    60   Output ~ 0
ASDQ4_TP1_[1..0]
Text HLabel 3750 5250 0    60   Output ~ 0
ASDQ4_TP2_[1..0]
$Comp
L ERF8-050-05.0-S-DV-K-TR J8
U 1 1 545BE30C
P 5950 4050
F 0 "J8" H 5950 6650 60  0000 C CNN
F 1 "ERF8-050-05.0-S-DV-K-TR" V 5950 4050 60  0000 C CNN
F 2 "~" H 6200 5350 60  0000 C CNN
F 3 "~" H 6200 5350 60  0000 C CNN
F 4 "SAM8622CT-ND" H 5950 4050 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 5950 4050 60  0001 C CNN "Field2"
	1    5950 4050
	1    0    0    -1  
$EndComp
Text HLabel 7400 6150 2    60   Output ~ 0
+1.4V[4..1]
Text HLabel 4500 6150 0    60   Input ~ 0
+1.4VRet[4..1]
Text HLabel 5100 3050 0    60   BiDi ~ 0
TEMP_1
Text HLabel 6800 3050 2    60   UnSpc ~ 0
TEMP_RET_1
Entry Wire Line
	4450 5650 4550 5550
Entry Wire Line
	4550 5650 4650 5550
Entry Wire Line
	4350 5450 4450 5350
Entry Wire Line
	4250 5450 4350 5350
Text Label 4350 5250 0    60   ~ 0
ASDQ3_TP1_1
Text Label 4450 5350 0    60   ~ 0
ASDQ3_TP1_0
Text Label 4550 5450 0    60   ~ 0
ASDQ3_TP2_1
Text Label 4650 5550 0    60   ~ 0
ASDQ3_TP2_0
Entry Wire Line
	3950 2350 4050 2450
Entry Wire Line
	3950 2450 4050 2550
Entry Wire Line
	3950 2550 4050 2650
Entry Wire Line
	3950 2650 4050 2750
Text Label 4050 2750 0    60   ~ 0
VRef_ASDQ4_0
Text Label 4050 2650 0    60   ~ 0
VRef_ASDQ4_1
Text Label 4050 2550 0    60   ~ 0
VRef_ASDQ4_2
Text Label 4050 2450 0    60   ~ 0
VRef_ASDQ4_3
Entry Wire Line
	7250 5550 7350 5650
Entry Wire Line
	7350 5550 7450 5650
Entry Wire Line
	7850 2450 7950 2350
Entry Wire Line
	7850 2550 7950 2450
Entry Wire Line
	7850 2650 7950 2550
Entry Wire Line
	7850 2750 7950 2650
Text Label 7850 2750 2    60   ~ 0
VRef_ASDQ2_0
Text Label 7850 2650 2    60   ~ 0
VRef_ASDQ2_1
Text Label 7850 2550 2    60   ~ 0
VRef_ASDQ2_2
Text Label 7850 2450 2    60   ~ 0
VRef_ASDQ2_3
Text Label 7250 5550 2    60   ~ 0
ASDQ1_TP2_0
Text Label 7350 5450 2    60   ~ 0
ASDQ1_TP2_1
Text Label 7450 5350 2    60   ~ 0
ASDQ1_TP1_0
Entry Wire Line
	7450 5350 7550 5450
Entry Wire Line
	7550 5350 7650 5450
Text Label 7550 5250 2    60   ~ 0
ASDQ1_TP1_1
Text Notes 2700 5450 2    60   ~ 0
0=N\n1=P
Text Notes 9300 5550 2    60   ~ 0
0=N\n1=P
Entry Wire Line
	7650 5150 7750 5250
Entry Wire Line
	7750 5150 7850 5250
Entry Wire Line
	7850 4950 7950 5050
Entry Wire Line
	7950 4950 8050 5050
Entry Wire Line
	7850 2050 7950 1950
Entry Wire Line
	7850 2150 7950 2050
Entry Wire Line
	7850 2250 7950 2150
Entry Wire Line
	7850 2350 7950 2250
Entry Wire Line
	3950 2250 4050 2350
Entry Wire Line
	3950 2150 4050 2250
Entry Wire Line
	3950 2050 4050 2150
Entry Wire Line
	3950 1950 4050 2050
Entry Wire Line
	3950 5050 4050 4950
Entry Wire Line
	3850 5050 3950 4950
Entry Wire Line
	4150 5250 4250 5150
Entry Wire Line
	4050 5250 4150 5150
Text Label 4050 2050 0    60   ~ 0
VRef_ASDQ3_3
Text Label 4050 2150 0    60   ~ 0
VRef_ASDQ3_2
Text Label 4050 2250 0    60   ~ 0
VRef_ASDQ3_1
Text Label 4050 2350 0    60   ~ 0
VRef_ASDQ3_0
Text Label 4150 5050 0    60   ~ 0
ASDQ4_TP2_1
Text Label 4250 5150 0    60   ~ 0
ASDQ4_TP2_0
Text Label 4050 4950 0    60   ~ 0
ASDQ4_TP1_0
Text Label 3950 4850 0    60   ~ 0
ASDQ4_TP1_1
Text Label 7950 4850 2    60   ~ 0
ASDQ2_TP1_1
Text Label 7850 4950 2    60   ~ 0
ASDQ2_TP1_0
Text Label 7750 5050 2    60   ~ 0
ASDQ2_TP2_1
Text Label 7650 5150 2    60   ~ 0
ASDQ2_TP2_0
Text Label 7850 2050 2    60   ~ 0
VRef_ASDQ1_3
Text Label 7850 2150 2    60   ~ 0
VRef_ASDQ1_2
Text Label 7850 2250 2    60   ~ 0
VRef_ASDQ1_1
Text Label 7850 2350 2    60   ~ 0
VRef_ASDQ1_0
Entry Wire Line
	3950 1750 4050 1850
Entry Wire Line
	3950 1850 4050 1950
Entry Wire Line
	3950 3050 4050 3150
Entry Wire Line
	3950 3150 4050 3250
Entry Wire Line
	3950 1550 4050 1650
Entry Wire Line
	3950 1650 4050 1750
Entry Wire Line
	3950 3250 4050 3350
Entry Wire Line
	3950 3350 4050 3450
Entry Wire Line
	7850 1650 7950 1550
Entry Wire Line
	7850 1750 7950 1650
Entry Wire Line
	7850 3150 7950 3050
Entry Wire Line
	7850 3250 7950 3150
Entry Wire Line
	7850 3450 7950 3350
Entry Wire Line
	7850 3350 7950 3250
Entry Wire Line
	7850 1950 7950 1850
Entry Wire Line
	7850 1850 7950 1750
Text Label 4050 1650 0    60   ~ 0
TDC3_C5_1
Text Label 4050 1750 0    60   ~ 0
TDC3_C5_0
Text Label 4050 3150 0    60   ~ 0
TDC3_8B10B_1
Text Label 4050 3250 0    60   ~ 0
TDC3_8B10B_0
Text Label 4050 3450 0    60   ~ 0
TDC4_8B10B_0
Text Label 4050 3350 0    60   ~ 0
TDC4_8B10B_1
Text Label 4050 1950 0    60   ~ 0
TDC4_C5_0
Text Label 4050 1850 0    60   ~ 0
TDC4_C5_1
Text Label 7850 1850 2    60   ~ 0
TDC2_C5_1
Text Label 7850 1950 2    60   ~ 0
TDC2_C5_0
Text Label 7850 3350 2    60   ~ 0
TDC2_8B10B_1
Text Label 7850 3450 2    60   ~ 0
TDC2_8B10B_0
Text Label 7850 3250 2    60   ~ 0
TDC1_8B10B_0
Text Label 7850 3150 2    60   ~ 0
TDC1_8B10B_1
Text Label 7850 1750 2    60   ~ 0
TDC1_C5_0
Text Label 7850 1650 2    60   ~ 0
TDC1_C5_1
Text Notes 3650 2900 0    60   ~ 0
0=ASDQ Onboard\n1=2X4 Pin Header
Text Notes 7550 2900 0    60   ~ 0
0=ASDQ Onboard\n1=2X4 Pin Header
Text HLabel 5100 2850 0    60   Input ~ 0
TDC_SCL
Text HLabel 6800 2850 2    60   Input ~ 0
TDC_SDA
Entry Wire Line
	7250 5950 7150 5850
Entry Wire Line
	7250 6150 7150 6050
Text Label 7150 6050 2    60   ~ 0
+1.4V4
Text Label 7150 5950 2    60   ~ 0
+1.4V3
Text Label 7150 5850 2    60   ~ 0
+1.4V2
Text Label 7150 5750 2    60   ~ 0
+1.4V1
Entry Wire Line
	7250 6050 7150 5950
Entry Wire Line
	7250 5850 7150 5750
Entry Wire Line
	8050 4750 7950 4650
Entry Wire Line
	8050 4650 7950 4550
Entry Wire Line
	8050 4550 7950 4450
Entry Wire Line
	8050 4450 7950 4350
Entry Wire Line
	8050 4350 7950 4250
Entry Wire Line
	8050 4250 7950 4150
Entry Wire Line
	8050 4150 7950 4050
Entry Wire Line
	8050 4050 7950 3950
Text Label 7950 4650 2    60   ~ 0
+3V4
Text Label 7950 4550 2    60   ~ 0
+3V3
Text Label 7950 4450 2    60   ~ 0
+3V2
Text Label 7950 4350 2    60   ~ 0
+3V1
Text Label 7950 4250 2    60   ~ 0
-3V4
Text Label 7950 4150 2    60   ~ 0
-3V3
Text Label 7950 4050 2    60   ~ 0
-3V2
Text Label 7950 3950 2    60   ~ 0
-3V1
Entry Wire Line
	4750 6050 4650 6150
Entry Wire Line
	4750 5950 4650 6050
Entry Wire Line
	4750 5850 4650 5950
Entry Wire Line
	4750 5750 4650 5850
Entry Wire Line
	3950 4650 3850 4750
Entry Wire Line
	3950 4550 3850 4650
Entry Wire Line
	3950 4450 3850 4550
Entry Wire Line
	3950 4350 3850 4450
Entry Wire Line
	3950 4250 3850 4350
Entry Wire Line
	3950 4150 3850 4250
Entry Wire Line
	3950 4050 3850 4150
Entry Wire Line
	3950 3950 3850 4050
Text Label 4750 6050 0    60   ~ 0
+1.4VRet4
Text Label 4750 5950 0    60   ~ 0
+1.4VRet3
Text Label 4750 5850 0    60   ~ 0
+1.4VRet2
Text Label 4750 5750 0    60   ~ 0
+1.4VRet1
Text Label 3950 4650 0    60   ~ 0
+3VRet4
Text Label 3950 4550 0    60   ~ 0
+3VRet3
Text Label 3950 4450 0    60   ~ 0
+3VRet2
Text Label 3950 4350 0    60   ~ 0
+3VRet1
Text Label 3950 4250 0    60   ~ 0
-3VRet4
Text Label 3950 4150 0    60   ~ 0
-3VRet3
Text Label 3950 4050 0    60   ~ 0
-3VRet2
Text Label 3950 3950 0    60   ~ 0
-3VRet1
Wire Wire Line
	6600 4250 7950 4250
Wire Wire Line
	6600 4550 7950 4550
Wire Wire Line
	6600 4450 7950 4450
Wire Bus Line
	3950 2350 3850 2350
Wire Bus Line
	3950 2350 3950 2650
Wire Wire Line
	4050 2750 5300 2750
Wire Wire Line
	4050 2650 5300 2650
Wire Wire Line
	4050 2550 5300 2550
Wire Wire Line
	4050 2450 5300 2450
Wire Bus Line
	7950 2350 8050 2350
Wire Bus Line
	7950 2350 7950 2650
Wire Wire Line
	6600 2750 7850 2750
Wire Wire Line
	6600 2650 7850 2650
Wire Wire Line
	6600 2550 7850 2550
Wire Wire Line
	6600 2450 7850 2450
Wire Wire Line
	7550 5250 7550 5350
Wire Wire Line
	3850 3750 5300 3750
Wire Wire Line
	3850 3650 5300 3650
Wire Wire Line
	3850 3850 5300 3850
Wire Wire Line
	3850 3550 5300 3550
Wire Wire Line
	6600 3750 8050 3750
Wire Wire Line
	6600 3650 8050 3650
Wire Wire Line
	6600 3850 8050 3850
Wire Wire Line
	6600 3550 8050 3550
Wire Bus Line
	8050 1950 7950 1950
Wire Bus Line
	7950 1950 7950 2250
Wire Wire Line
	6600 2050 7850 2050
Wire Wire Line
	6600 2150 7850 2150
Wire Wire Line
	6600 2250 7850 2250
Wire Wire Line
	6600 2350 7850 2350
Wire Bus Line
	3850 1950 3950 1950
Wire Bus Line
	3950 1950 3950 2250
Wire Wire Line
	4050 2050 5300 2050
Wire Wire Line
	4050 2150 5300 2150
Wire Wire Line
	4050 2250 5300 2250
Wire Wire Line
	4050 2350 5300 2350
Wire Bus Line
	3850 1750 3950 1750
Wire Bus Line
	3950 1750 3950 1850
Wire Bus Line
	3850 3050 3950 3050
Wire Bus Line
	3950 3050 3950 3150
Wire Bus Line
	3850 1550 3950 1550
Wire Bus Line
	3950 1550 3950 1650
Wire Bus Line
	3850 3250 3950 3250
Wire Bus Line
	3950 3250 3950 3350
Wire Bus Line
	8050 1550 7950 1550
Wire Bus Line
	7950 1550 7950 1650
Wire Bus Line
	8050 3050 7950 3050
Wire Bus Line
	7950 3050 7950 3150
Wire Bus Line
	8050 1750 7950 1750
Wire Bus Line
	7950 1750 7950 1850
Wire Bus Line
	8050 3250 7950 3250
Wire Bus Line
	7950 3250 7950 3350
Wire Wire Line
	4050 1950 5300 1950
Wire Wire Line
	4050 1850 5300 1850
Wire Wire Line
	4050 3350 5300 3350
Wire Wire Line
	4050 3450 5300 3450
Wire Wire Line
	4050 3250 5300 3250
Wire Wire Line
	4050 3150 5300 3150
Wire Wire Line
	4050 1750 5300 1750
Wire Wire Line
	4050 1650 5300 1650
Wire Wire Line
	6600 1950 7850 1950
Wire Wire Line
	6600 1850 7850 1850
Wire Wire Line
	6600 3350 7850 3350
Wire Wire Line
	6600 3450 7850 3450
Wire Wire Line
	6600 3250 7850 3250
Wire Wire Line
	6600 3150 7850 3150
Wire Wire Line
	6600 1750 7850 1750
Wire Wire Line
	6600 1650 7850 1650
Wire Wire Line
	5100 3050 5300 3050
Wire Wire Line
	5100 2950 5300 2950
Wire Wire Line
	6600 3050 6800 3050
Wire Wire Line
	6600 2950 6950 2950
Wire Wire Line
	5300 2850 5100 2850
Wire Bus Line
	7250 6150 7400 6150
Wire Bus Line
	7250 5850 7250 6150
Wire Bus Line
	8050 4750 8150 4750
Wire Bus Line
	8050 4450 8050 4750
Wire Bus Line
	8050 4350 8150 4350
Wire Bus Line
	8050 4050 8050 4350
Wire Wire Line
	6600 4150 7950 4150
Wire Wire Line
	6600 4050 7950 4050
Wire Wire Line
	6600 3950 7950 3950
Wire Wire Line
	6600 4650 7950 4650
Wire Wire Line
	6600 4350 7950 4350
Wire Wire Line
	6600 6050 7150 6050
Wire Wire Line
	6600 5950 7150 5950
Wire Wire Line
	6600 5850 7150 5850
Wire Wire Line
	6600 5750 7150 5750
Wire Bus Line
	4650 6150 4500 6150
Wire Bus Line
	4650 5850 4650 6150
Wire Bus Line
	3850 4750 3750 4750
Wire Bus Line
	3850 4450 3850 4750
Wire Bus Line
	3850 4350 3750 4350
Wire Bus Line
	3850 4050 3850 4350
Wire Wire Line
	3950 4250 5300 4250
Wire Wire Line
	3950 4150 5300 4150
Wire Wire Line
	3950 4050 5300 4050
Wire Wire Line
	3950 3950 5300 3950
Wire Wire Line
	3950 4650 5300 4650
Wire Wire Line
	3950 4550 5300 4550
Wire Wire Line
	3950 4450 5300 4450
Wire Wire Line
	3950 4350 5300 4350
Wire Wire Line
	5300 6050 4750 6050
Wire Wire Line
	5300 5950 4750 5950
Wire Wire Line
	5300 5850 4750 5850
Wire Wire Line
	5300 5750 4750 5750
Wire Wire Line
	4650 5550 5300 5550
Wire Wire Line
	4550 5550 4550 5450
Wire Wire Line
	4550 5450 5300 5450
Wire Wire Line
	4450 5350 5300 5350
Wire Wire Line
	4350 5350 4350 5250
Wire Wire Line
	4350 5250 5300 5250
Wire Bus Line
	3750 5450 4350 5450
Wire Bus Line
	3750 5650 4550 5650
Wire Wire Line
	4250 5150 5300 5150
Wire Wire Line
	4150 5150 4150 5050
Wire Wire Line
	4150 5050 5300 5050
Wire Wire Line
	4050 4950 5300 4950
Wire Wire Line
	3950 4950 3950 4850
Wire Wire Line
	3950 4850 5300 4850
Wire Bus Line
	3750 5250 4150 5250
Wire Bus Line
	3750 5050 3950 5050
Wire Wire Line
	6600 2850 6800 2850
Wire Bus Line
	7950 5050 8150 5050
Wire Bus Line
	7750 5250 8150 5250
Wire Wire Line
	6600 5150 7650 5150
Wire Wire Line
	7750 5050 7750 5150
Wire Wire Line
	6600 5050 7750 5050
Wire Wire Line
	6600 4950 7850 4950
Wire Wire Line
	7950 4850 7950 4950
Wire Wire Line
	6600 4850 7950 4850
Wire Wire Line
	7550 5250 6600 5250
Wire Wire Line
	7450 5350 6600 5350
Wire Wire Line
	7350 5550 7350 5450
Wire Wire Line
	7350 5450 6600 5450
Wire Wire Line
	7250 5550 6600 5550
Wire Bus Line
	7350 5650 8150 5650
Wire Bus Line
	7550 5450 8150 5450
Wire Wire Line
	6600 6550 6900 6550
Wire Wire Line
	6600 6150 6800 6150
Wire Wire Line
	6800 6150 6800 6550
Connection ~ 6800 6550
Wire Wire Line
	6600 6250 6800 6250
Connection ~ 6800 6250
Wire Wire Line
	6600 6350 6800 6350
Connection ~ 6800 6350
Wire Wire Line
	6600 6450 6800 6450
Connection ~ 6800 6450
Wire Wire Line
	4900 6550 5300 6550
Wire Wire Line
	5100 6150 5300 6150
Wire Wire Line
	5100 6150 5100 6550
Connection ~ 5100 6550
Wire Wire Line
	5100 6250 5300 6250
Connection ~ 5100 6250
Wire Wire Line
	5300 6350 5100 6350
Connection ~ 5100 6350
Wire Wire Line
	5300 6450 5100 6450
Connection ~ 5100 6450
Text Label 9200 5900 0    60   ~ 0
+1.4V1
Text Label 9200 6000 0    60   ~ 0
+1.4V2
Text Label 9200 6100 0    60   ~ 0
+1.4V3
Text Label 9200 6200 0    60   ~ 0
+1.4V4
Text Label 9200 6400 0    60   ~ 0
+1.4VRet1
Text Label 9200 6500 0    60   ~ 0
+1.4VRet2
Text Label 9200 6600 0    60   ~ 0
+1.4VRet3
Text Label 9200 6700 0    60   ~ 0
+1.4VRet4
Wire Wire Line
	9100 6700 9200 6700
Wire Wire Line
	9100 6400 9100 6700
Wire Wire Line
	9100 6600 9200 6600
Wire Wire Line
	9100 6500 9200 6500
Connection ~ 9100 6600
Wire Wire Line
	9100 6400 9200 6400
Connection ~ 9100 6500
Wire Wire Line
	9100 6200 9200 6200
Wire Wire Line
	9100 5900 9100 6200
Wire Wire Line
	9100 6100 9200 6100
Wire Wire Line
	9100 6000 9200 6000
Connection ~ 9100 6100
Wire Wire Line
	9100 5900 9200 5900
Connection ~ 9100 6000
Text Label 6650 4750 0    60   ~ 0
TSTP_shield
Text Label 6650 5650 0    60   ~ 0
TSTP_shield
Text Label 5250 5650 2    60   ~ 0
TSTP_shield
Text Label 5250 4750 2    60   ~ 0
TSTP_shield
Wire Wire Line
	4750 4750 5300 4750
Wire Wire Line
	4750 5650 5300 5650
Wire Wire Line
	7150 4750 6600 4750
Wire Wire Line
	7150 5650 6600 5650
Text Label 6950 2950 2    60   ~ 0
Vref_gnd
Wire Wire Line
	9600 2850 10000 2850
Text Label 9950 2850 2    60   ~ 0
Vref_gnd
Text Label 9450 2950 0    60   ~ 0
TSTP_shield
Wire Wire Line
	10000 2950 9400 2950
Wire Wire Line
	10000 2850 10000 2950
Wire Wire Line
	10000 2900 10050 2900
Connection ~ 10000 2900
$EndSCHEMATC
