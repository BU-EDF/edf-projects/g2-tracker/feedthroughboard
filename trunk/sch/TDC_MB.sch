EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:erf8-050-05
LIBS:feedthrough-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title ""
Date "12 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4000 2350 0    60   UnSpc ~ 0
ASDQ1_ShieldA
Text HLabel 7700 2350 2    60   UnSpc ~ 0
ASDQ2_ShieldA
Text HLabel 9400 3950 2    60   Input ~ 0
ASDQ2_AP[7..0]
Text HLabel 9400 4050 2    60   Input ~ 0
ASDQ2_AN[7..0]
Text HLabel 2300 4050 0    60   Input ~ 0
ASDQ1_AN[7..0]
Text HLabel 2300 3950 0    60   Input ~ 0
ASDQ1_AP[7..0]
Text HLabel 9400 1800 2    60   Input ~ 0
TDC2_C5_[1..0]
Text HLabel 9400 2000 2    60   Output ~ 0
TDC2_8b10b_[1..0]
Text HLabel 2300 2000 0    60   Output ~ 0
TDC1_8b10b_[1..0]
Text HLabel 2300 1800 0    60   Input ~ 0
TDC1_C5_[1..0]
Text HLabel 4800 6550 0    60   Input ~ 0
+4VRet
Text HLabel 4800 6250 0    60   Input ~ 0
+4V
Text HLabel 6950 6550 2    60   Input ~ 0
JTAG_TMS
Text HLabel 6950 6450 2    60   Output ~ 0
JTAG_TDO
Text HLabel 6950 6350 2    60   Input ~ 0
JTAG_TDI
Text HLabel 6950 6250 2    60   Input ~ 0
JTAG_TCK
Text HLabel 4800 4350 0    60   UnSpc ~ 0
ASDQ1_ShieldB
Text HLabel 6900 4350 2    60   UnSpc ~ 0
ASDQ2_ShieldB
Text HLabel 2300 5950 0    60   Input ~ 0
ASDQ1_BP[7..0]
Text HLabel 2300 6050 0    60   Input ~ 0
ASDQ1_BN[7..0]
Text HLabel 9400 5950 2    60   Input ~ 0
ASDQ2_BP[7..0]
Text HLabel 9400 6050 2    60   Input ~ 0
ASDQ2_BN[7..0]
$Comp
L ERF8-050-05.0-S-DV-K-TR J5
U 1 1 54515895
P 5850 4150
AR Path="/542B54BD/54515895" Ref="J5"  Part="1" 
AR Path="/542B4354/54515895" Ref="J7"  Part="1" 
F 0 "J7" H 5800 850 60  0000 C CNN
F 1 "ERF8-050-05.0-S-DV-K-TR" V 5850 4100 60  0000 C CNN
F 2 "~" H 6100 4150 60  0000 C CNN
F 3 "~" H 6100 4150 60  0000 C CNN
F 4 "SAM8622CT-ND" H 5850 4150 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 5850 4150 60  0001 C CNN "Field2"
	1    5850 4150
	1    0    0    -1  
$EndComp
Text Label 4800 4250 2    60   ~ 0
ASDQ1_ShieldB
Text Label 4800 6150 2    60   ~ 0
ASDQ1_ShieldB
Entry Wire Line
	2500 5750 2600 5650
Entry Wire Line
	2500 5550 2600 5450
Entry Wire Line
	2500 5350 2600 5250
Text Label 2600 5850 0    60   ~ 0
ASDQ1_BP7
Entry Wire Line
	2500 5150 2600 5050
Entry Wire Line
	2500 4950 2600 4850
Entry Wire Line
	2500 4750 2600 4650
Entry Wire Line
	2500 4550 2600 4450
Entry Wire Line
	2500 5950 2600 5850
Text Label 2600 5650 0    60   ~ 0
ASDQ1_BP6
Text Label 2600 5450 0    60   ~ 0
ASDQ1_BP5
Text Label 2600 5250 0    60   ~ 0
ASDQ1_BP4
Text Label 2600 5050 0    60   ~ 0
ASDQ1_BP3
Text Label 2600 4850 0    60   ~ 0
ASDQ1_BP2
Text Label 2600 4650 0    60   ~ 0
ASDQ1_BP1
Text Label 2600 4450 0    60   ~ 0
ASDQ1_BP0
Text Label 4600 5950 0    60   ~ 0
ASDQ1_BN7
Entry Wire Line
	4500 6050 4600 5950
Entry Wire Line
	4300 6050 4400 5950
Entry Wire Line
	4100 6050 4200 5950
Entry Wire Line
	3900 6050 4000 5950
Entry Wire Line
	3700 6050 3800 5950
Entry Wire Line
	3500 6050 3600 5950
Entry Wire Line
	3300 6050 3400 5950
Text Label 4400 5750 0    60   ~ 0
ASDQ1_BN6
Entry Wire Line
	3100 6050 3200 5950
Text Label 4200 5550 0    60   ~ 0
ASDQ1_BN5
Text Label 4000 5350 0    60   ~ 0
ASDQ1_BN4
Text Label 3800 5150 0    60   ~ 0
ASDQ1_BN3
Text Label 3600 4950 0    60   ~ 0
ASDQ1_BN2
Text Label 3400 4750 0    60   ~ 0
ASDQ1_BN1
Text Label 3200 4550 0    60   ~ 0
ASDQ1_BN0
Text Label 4800 4150 2    60   ~ 0
ASDQ1_ShieldA
Entry Wire Line
	4500 4050 4600 3950
Text Label 4600 3950 0    60   ~ 0
ASDQ1_AN7
Entry Wire Line
	4300 4050 4400 3950
Entry Wire Line
	4100 4050 4200 3950
Entry Wire Line
	3900 4050 4000 3950
Entry Wire Line
	3700 4050 3800 3950
Entry Wire Line
	3500 4050 3600 3950
Entry Wire Line
	3300 4050 3400 3950
Entry Wire Line
	3100 4050 3200 3950
Text Label 4400 3750 0    60   ~ 0
ASDQ1_AN6
Text Label 4200 3550 0    60   ~ 0
ASDQ1_AN5
Text Label 4000 3350 0    60   ~ 0
ASDQ1_AN4
Text Label 3800 3150 0    60   ~ 0
ASDQ1_AN3
Text Label 3600 2950 0    60   ~ 0
ASDQ1_AN2
Text Label 3400 2750 0    60   ~ 0
ASDQ1_AN1
Text Label 3200 2550 0    60   ~ 0
ASDQ1_AN0
Entry Wire Line
	2500 3750 2600 3650
Entry Wire Line
	2500 3550 2600 3450
Entry Wire Line
	2500 3350 2600 3250
Entry Wire Line
	2500 3150 2600 3050
Entry Wire Line
	2500 2950 2600 2850
Entry Wire Line
	2500 2750 2600 2650
Entry Wire Line
	2500 2550 2600 2450
Entry Wire Line
	2500 3950 2600 3850
Text Label 2600 3850 0    60   ~ 0
ASDQ1_AP7
Text Label 2600 3650 0    60   ~ 0
ASDQ1_AP6
Text Label 2600 3450 0    60   ~ 0
ASDQ1_AP5
Text Label 2600 3250 0    60   ~ 0
ASDQ1_AP4
Text Label 2600 3050 0    60   ~ 0
ASDQ1_AP3
Text Label 2600 2850 0    60   ~ 0
ASDQ1_AP2
Text Label 2600 2650 0    60   ~ 0
ASDQ1_AP1
Text Label 2600 2450 0    60   ~ 0
ASDQ1_AP0
Text Label 4800 2350 2    60   ~ 0
ASDQ1_ShieldA
Text Label 6950 6150 0    60   ~ 0
ASDQ2_ShieldB
Entry Wire Line
	7100 5950 7200 6050
Entry Wire Line
	7300 5950 7400 6050
Entry Wire Line
	7500 5950 7600 6050
Entry Wire Line
	7700 5950 7800 6050
Entry Wire Line
	7900 5950 8000 6050
Entry Wire Line
	8100 5950 8200 6050
Entry Wire Line
	8300 5950 8400 6050
Entry Wire Line
	8500 5950 8600 6050
Text Label 7100 5950 2    60   ~ 0
ASDQ2_BN7
Text Label 7300 5750 2    60   ~ 0
ASDQ2_BN6
Text Label 7500 5550 2    60   ~ 0
ASDQ2_BN5
Text Label 7700 5350 2    60   ~ 0
ASDQ2_BN4
Text Label 7900 5150 2    60   ~ 0
ASDQ2_BN3
Text Label 8100 4950 2    60   ~ 0
ASDQ2_BN2
Text Label 8300 4750 2    60   ~ 0
ASDQ2_BN1
Text Label 8500 4550 2    60   ~ 0
ASDQ2_BN0
Entry Wire Line
	9100 5850 9200 5950
Entry Wire Line
	9100 5650 9200 5750
Entry Wire Line
	9100 5450 9200 5550
Entry Wire Line
	9100 5250 9200 5350
Entry Wire Line
	9100 5050 9200 5150
Entry Wire Line
	9100 4850 9200 4950
Entry Wire Line
	9100 4650 9200 4750
Entry Wire Line
	9100 4450 9200 4550
Text Label 9100 5850 2    60   ~ 0
ASDQ2_BP7
Text Label 9100 5650 2    60   ~ 0
ASDQ2_BP6
Text Label 9100 5450 2    60   ~ 0
ASDQ2_BP5
Text Label 9100 5250 2    60   ~ 0
ASDQ2_BP4
Text Label 9100 5050 2    60   ~ 0
ASDQ2_BP3
Text Label 9100 4850 2    60   ~ 0
ASDQ2_BP2
Text Label 9100 4650 2    60   ~ 0
ASDQ2_BP1
Text Label 9100 4450 2    60   ~ 0
ASDQ2_BP0
Text Label 6900 4250 0    60   ~ 0
ASDQ2_ShieldB
Text Label 6900 4150 0    60   ~ 0
ASDQ2_ShieldA
Entry Wire Line
	7100 3950 7200 4050
Entry Wire Line
	7300 3950 7400 4050
Entry Wire Line
	7500 3950 7600 4050
Entry Wire Line
	7700 3950 7800 4050
Entry Wire Line
	7900 3950 8000 4050
Entry Wire Line
	8100 3950 8200 4050
Entry Wire Line
	8300 3950 8400 4050
Entry Wire Line
	8500 3950 8600 4050
Entry Wire Line
	9100 3850 9200 3950
Entry Wire Line
	9100 3650 9200 3750
Entry Wire Line
	9100 3450 9200 3550
Entry Wire Line
	9100 3250 9200 3350
Entry Wire Line
	9100 3050 9200 3150
Entry Wire Line
	9100 2850 9200 2950
Entry Wire Line
	9100 2650 9200 2750
Entry Wire Line
	9100 2450 9200 2550
Text Label 7100 3950 2    60   ~ 0
ASDQ2_AN7
Text Label 7300 3750 2    60   ~ 0
ASDQ2_AN6
Text Label 7500 3550 2    60   ~ 0
ASDQ2_AN5
Text Label 7700 3350 2    60   ~ 0
ASDQ2_AN4
Text Label 7900 3150 2    60   ~ 0
ASDQ2_AN3
Text Label 8100 2950 2    60   ~ 0
ASDQ2_AN2
Text Label 8300 2750 2    60   ~ 0
ASDQ2_AN1
Text Label 8500 2550 2    60   ~ 0
ASDQ2_AN0
Text Label 9100 3850 2    60   ~ 0
ASDQ2_AP7
Text Label 9100 3650 2    60   ~ 0
ASDQ2_AP6
Text Label 9100 3450 2    60   ~ 0
ASDQ2_AP5
Text Label 9100 3250 2    60   ~ 0
ASDQ2_AP4
Text Label 9100 3050 2    60   ~ 0
ASDQ2_AP3
Text Label 9100 2850 2    60   ~ 0
ASDQ2_AP2
Text Label 9100 2650 2    60   ~ 0
ASDQ2_AP1
Text Label 9100 2450 2    60   ~ 0
ASDQ2_AP0
Text Label 6900 2350 0    60   ~ 0
ASDQ2_ShieldA
Entry Wire Line
	2600 1650 2700 1750
Text Label 2700 1750 0    60   ~ 0
TDC1_C5_1
Entry Wire Line
	2400 1650 2500 1750
Text Label 2500 1850 0    60   ~ 0
TDC1_C5_0
Entry Wire Line
	2400 2150 2500 2050
Entry Wire Line
	2600 2150 2700 2050
Text Label 2500 1950 0    60   ~ 0
TDC1_8b10b_1
Text Label 2700 2050 0    60   ~ 0
TDC1_8b10b_0
Entry Wire Line
	9000 2050 9100 2150
Entry Wire Line
	9200 2050 9300 2150
Entry Wire Line
	9200 1750 9300 1650
Entry Wire Line
	9000 1750 9100 1650
Text Label 9000 1750 2    60   ~ 0
TDC2_C5_1
Text Label 9200 1850 2    60   ~ 0
TDC2_C5_0
Text Label 9200 1950 2    60   ~ 0
TDC2_8b10b_1
Text Label 9000 2050 2    60   ~ 0
TDC2_8b10b_0
Text Notes 10650 1900 2    60   ~ 0
1=P\n0=N
Text HLabel 7700 2150 2    60   Input ~ 0
TDC_SDA
Text Notes 1250 1900 2    60   ~ 0
1=P\n0=N
Wire Wire Line
	2600 5850 5200 5850
Wire Wire Line
	2600 5650 5200 5650
Wire Wire Line
	2600 5450 5200 5450
Wire Wire Line
	4000 2350 5200 2350
Wire Wire Line
	2600 5250 5200 5250
Wire Wire Line
	2600 5050 5200 5050
Wire Wire Line
	2600 4850 5200 4850
Wire Wire Line
	2600 4650 5200 4650
Wire Wire Line
	2600 4450 5200 4450
Wire Wire Line
	4600 5950 5200 5950
Wire Bus Line
	2300 6050 4500 6050
Wire Wire Line
	4400 5750 4400 5950
Wire Wire Line
	5200 5550 4200 5550
Wire Wire Line
	4200 5550 4200 5950
Wire Wire Line
	5200 5350 4000 5350
Wire Wire Line
	4000 5350 4000 5950
Wire Wire Line
	5200 5150 3800 5150
Wire Wire Line
	3800 5150 3800 5950
Wire Wire Line
	5200 4950 3600 4950
Wire Wire Line
	3600 4950 3600 5950
Wire Wire Line
	5200 4750 3400 4750
Wire Wire Line
	3400 4750 3400 5950
Wire Wire Line
	5200 4550 3200 4550
Wire Wire Line
	3200 4550 3200 5950
Wire Bus Line
	2300 4050 4500 4050
Wire Wire Line
	4600 3950 5200 3950
Wire Wire Line
	4400 3950 4400 3750
Wire Wire Line
	4400 3750 5200 3750
Wire Wire Line
	4200 3950 4200 3550
Wire Wire Line
	4200 3550 5200 3550
Wire Wire Line
	4000 3950 4000 3350
Wire Wire Line
	4000 3350 5200 3350
Wire Wire Line
	3800 3950 3800 3150
Wire Wire Line
	3800 3150 5200 3150
Wire Wire Line
	3600 3950 3600 2950
Wire Wire Line
	3600 2950 5200 2950
Wire Wire Line
	3400 3950 3400 2750
Wire Wire Line
	3400 2750 5200 2750
Wire Wire Line
	3200 3950 3200 2550
Wire Wire Line
	3200 2550 5200 2550
Wire Bus Line
	2500 2550 2500 3950
Wire Wire Line
	2600 3850 5200 3850
Wire Wire Line
	2600 3650 5200 3650
Wire Wire Line
	2600 3450 5200 3450
Wire Wire Line
	2600 3250 5200 3250
Wire Wire Line
	2600 3050 5200 3050
Wire Wire Line
	2600 2850 5200 2850
Wire Wire Line
	2600 2650 5200 2650
Wire Wire Line
	2600 2450 5200 2450
Wire Wire Line
	6500 2350 7700 2350
Wire Wire Line
	6500 2250 6700 2250
Wire Bus Line
	7200 6050 9400 6050
Wire Wire Line
	7100 5950 6500 5950
Wire Wire Line
	7300 5950 7300 5750
Wire Wire Line
	7300 5750 6500 5750
Wire Wire Line
	7500 5950 7500 5550
Wire Wire Line
	7500 5550 6500 5550
Wire Wire Line
	7700 5950 7700 5350
Wire Wire Line
	7700 5350 6500 5350
Wire Wire Line
	7900 5950 7900 5150
Wire Wire Line
	7900 5150 6500 5150
Wire Wire Line
	8100 5950 8100 4950
Wire Wire Line
	8100 4950 6500 4950
Wire Wire Line
	8300 5950 8300 4750
Wire Wire Line
	8300 4750 6500 4750
Wire Wire Line
	8500 5950 8500 4550
Wire Wire Line
	8500 4550 6500 4550
Wire Bus Line
	9200 4550 9200 5950
Wire Wire Line
	9100 5850 6500 5850
Wire Wire Line
	6500 5650 9100 5650
Wire Wire Line
	9100 5450 6500 5450
Wire Wire Line
	6500 5250 9100 5250
Wire Wire Line
	9100 5050 6500 5050
Wire Wire Line
	6500 4850 9100 4850
Wire Wire Line
	9100 4650 6500 4650
Wire Wire Line
	6500 4450 9100 4450
Wire Wire Line
	6500 4350 6900 4350
Wire Bus Line
	7200 4050 9400 4050
Wire Wire Line
	7100 3950 6500 3950
Wire Wire Line
	6500 3750 7300 3750
Wire Wire Line
	7300 3750 7300 3950
Wire Wire Line
	7500 3950 7500 3550
Wire Wire Line
	7500 3550 6500 3550
Wire Wire Line
	7700 3950 7700 3350
Wire Wire Line
	7700 3350 6500 3350
Wire Wire Line
	7900 3950 7900 3150
Wire Wire Line
	7900 3150 6500 3150
Wire Wire Line
	6500 2950 8100 2950
Wire Wire Line
	8100 2950 8100 3950
Wire Wire Line
	6500 2750 8300 2750
Wire Wire Line
	8300 2750 8300 3950
Wire Wire Line
	8500 3950 8500 2550
Wire Wire Line
	8500 2550 6500 2550
Wire Wire Line
	9100 3850 6500 3850
Wire Wire Line
	6500 3650 9100 3650
Wire Wire Line
	9100 3450 6500 3450
Wire Wire Line
	6500 3250 9100 3250
Wire Wire Line
	9100 3050 6500 3050
Wire Wire Line
	6500 2850 9100 2850
Wire Wire Line
	9100 2650 6500 2650
Wire Wire Line
	6500 2450 9100 2450
Wire Wire Line
	4800 6250 5200 6250
Wire Wire Line
	5200 6350 5000 6350
Wire Wire Line
	5000 6250 5000 6450
Connection ~ 5000 6250
Wire Wire Line
	4800 6550 5200 6550
Wire Wire Line
	5200 6650 5000 6650
Wire Wire Line
	5000 6550 5000 6800
Connection ~ 5000 6550
Wire Wire Line
	6500 6250 6950 6250
Wire Wire Line
	6500 6350 6950 6350
Wire Wire Line
	6500 6450 6950 6450
Wire Wire Line
	6500 6550 6950 6550
Wire Wire Line
	2700 1750 5200 1750
Wire Bus Line
	2300 1650 2600 1650
Wire Wire Line
	2500 1750 2500 1850
Wire Wire Line
	2500 1850 5200 1850
Wire Bus Line
	2300 2150 2600 2150
Wire Wire Line
	2500 2050 2500 1950
Wire Wire Line
	2500 1950 5200 1950
Wire Wire Line
	2700 2050 5200 2050
Wire Bus Line
	9100 2150 9400 2150
Wire Bus Line
	9100 1650 9400 1650
Wire Wire Line
	6500 2050 9000 2050
Wire Wire Line
	9200 1950 9200 2050
Wire Wire Line
	6500 1950 9200 1950
Wire Wire Line
	6500 1750 9000 1750
Wire Wire Line
	9200 1850 9200 1750
Wire Wire Line
	6500 1850 9200 1850
Wire Bus Line
	2500 3950 2300 3950
Wire Bus Line
	2500 5950 2300 5950
Wire Bus Line
	2500 4550 2500 5950
Wire Bus Line
	9200 5950 9400 5950
Wire Bus Line
	9200 3950 9400 3950
Wire Bus Line
	9200 2550 9200 3950
Wire Wire Line
	6500 6650 6700 6650
Wire Wire Line
	5000 6800 6700 6800
Connection ~ 5000 6650
Wire Wire Line
	6700 6800 6700 6650
Wire Wire Line
	5200 5750 4400 5750
Wire Bus Line
	2300 1650 2300 1800
Wire Bus Line
	2300 2150 2300 2000
Wire Bus Line
	9400 2150 9400 2000
Wire Bus Line
	9400 1650 9400 1800
Wire Wire Line
	5000 6450 5200 6450
Connection ~ 5000 6350
Wire Wire Line
	4800 6150 5200 6150
Wire Wire Line
	5200 6050 5000 6050
Wire Wire Line
	5000 6050 5000 6150
Connection ~ 5000 6150
Wire Wire Line
	6500 6150 6950 6150
Wire Wire Line
	6500 6050 6700 6050
Wire Wire Line
	6700 6050 6700 6150
Connection ~ 6700 6150
Wire Wire Line
	4800 4350 5200 4350
Wire Wire Line
	4800 4250 4900 4250
Wire Wire Line
	4900 4250 4900 4350
Connection ~ 4900 4350
Wire Wire Line
	5100 4350 5100 4250
Wire Wire Line
	5100 4250 5200 4250
Connection ~ 5100 4350
Wire Wire Line
	6600 4350 6600 4250
Wire Wire Line
	6600 4250 6500 4250
Connection ~ 6600 4350
Wire Wire Line
	6900 4250 6800 4250
Wire Wire Line
	6800 4250 6800 4350
Connection ~ 6800 4350
Wire Wire Line
	6500 4150 6900 4150
Wire Wire Line
	6500 4050 6700 4050
Wire Wire Line
	6700 4050 6700 4150
Connection ~ 6700 4150
Wire Wire Line
	4800 4150 5200 4150
Wire Wire Line
	5200 4050 5000 4050
Wire Wire Line
	5000 4050 5000 4150
Connection ~ 5000 4150
Wire Wire Line
	5200 2250 5000 2250
Wire Wire Line
	5000 2250 5000 2350
Connection ~ 5000 2350
Wire Wire Line
	6700 2250 6700 2350
Connection ~ 6700 2350
Wire Wire Line
	6500 2150 7700 2150
Wire Wire Line
	5200 2150 4900 2150
Text Notes 4200 1600 0    60   ~ 0
Pin 9 GND for board 0 only
Text HLabel 4900 2150 0    60   Input ~ 0
TDC_SCL
$EndSCHEMATC
