
HOWTO generate gerbers:

Check only options:
  "Plot module value on silkscreen"
  "Plot module reference on silkscreen"
  "Plot other module texts on silkscreen"

Check layers:
  F.Cu
  Inner1.Cu throgh Inner6.Cu
  B.Cu
  F.Paste, F.Silks, F.Mask
  B.Paste, B.Silks, B.Mask
  Cmts.User

Run the generate gerbers, generate drills
and generate drill maps.

Go to gerber directory, run make_fab.sh

zip -r G2-FT-RevX.zip G2-FT* README.txt





Layout Mechanics:

Board outline 93w x 87h.
Mounting holes defined by DXF

Connector center point defined as the midpoint of a line
segment drawn between diagonally opposite PCB pads
(e.g. between pins 1 and 100 on the ERF8 connectors)

For the ERF8 this is also the geometry center of the
46 x 5.6 body.

For the HV connector the center is the location of pin 13.

The origin of the PCB is defined as the upper left corner,
with +x to the right and +y downwards.

Component locations for air side:
     
     PCB X  PCB Y      DXF X  DXF Y
J8   53.40  16.98      53.00  17.00
J7   53.40  35.48      53.00  35.50
J5   53.40  53.99      53.00  54.00
J10  46.50  69.22      46.50  69.22

Component locations for gas side:

J2   53.00  31.39      53.00  31.20
J6   53.00  39.39      53.00  39.20
J3   53.00  49.89      53.00  49.70
J4   53.00  57.89      53.00  57.70
J9   46.50  69.22      46.50  69.22

